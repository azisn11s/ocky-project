<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
  {!! Form::label('title', 'Judul', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('title', null, ['class'=>'form-control']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {!! $errors->has('author_id') ? 'has-error' : '' !!}">
  {!! Form::label('author_id', 'Penulis', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
{!! Form::select('author_id', [''=>'']+App\Author::pluck('name','id')->all(), null, [
  'class'=>'js-selectize',
  'placeholder' => 'Pilih Penulis']) 
!!}
    {!! $errors->first('author_id', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {!! $errors->has('category_id') ? 'has-error' : '' !!}">
  {!! Form::label('category_id', 'Kategori', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
{!! Form::select('category_id', [''=>'']+App\Category::pluck('name','id')->all(), null, [
  'class'=>'js-selectize',
  'placeholder' => 'Pilih Kategori']) 
!!}
    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group {!! $errors->has('publisher_id') ? 'has-error' : '' !!}">
  {!! Form::label('publisher_id', 'Penerbit', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
{!! Form::select('publisher_id', [''=>'']+App\Publisher::pluck('name','id')->all(), null, [
  'class'=>'js-selectize',
  'placeholder' => 'Pilih Penerbit']) 
!!}
    {!! $errors->first('publisher_id', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('isbn') ? ' has-error' : '' }}">
  {!! Form::label('isbn', 'ISBN', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('isbn', null, ['class'=>'form-control']) !!}
    {!! $errors->first('isbn', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('publish_place') ? ' has-error' : '' }}">
  {!! Form::label('publish_place', 'Diterbitkan di', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('publish_place', null, ['class'=>'form-control']) !!}
    {!! $errors->first('publish_place', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('publish_date') ? ' has-error' : '' }}">
  {!! Form::label('publish_date', 'Tgl. Terbit', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::text('publish_date', '', ['id' => 'datepicker', 'class'=>'form-control']) !!}
    {{-- {!! Form::date('publish_date', \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $book->publish_date), ['id' => 'datepicker_', 'class'=>'form-control']) !!} --}}
    {!! $errors->first('publish_date', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('cover') ? ' has-error' : '' }}">
  {!! Form::label('cover', 'Cover', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::file('cover') !!}
    @if (isset($book) && $book->cover)
      <p>
      {!! Html::image(asset('img/'.$book->cover), null, ['class'=>'img-rounded img-responsive']) !!}
      </p>
    @endif
    {!! $errors->first('cover', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('pdf_file') ? ' has-error' : '' }}">
  {!! Form::label('pdf_file', 'File PDF', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-4">
    {!! Form::file('pdf_file') !!}

    {!! $errors->first('pdf_file', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('total_page') ? ' has-error' : '' }}">
  {!! Form::label('total_page', 'Jumlah Halaman', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-2">
    {!! Form::number('total_page', null, ['class'=>'form-control']) !!}
    {!! $errors->first('total_page', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
  {!! Form::label('description', 'Deskripsi', ['class'=>'col-md-2 control-label']) !!}
  <div class="col-md-5">
    {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group">
  <div class="col-md-4 col-md-offset-2">
    {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
  </div>
</div>
