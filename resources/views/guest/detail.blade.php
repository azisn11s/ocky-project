@extends('layouts.app')

@section('content')
  <div class="container">
    {{-- Book Title --}}
    <div class="row">
      <div class="book-title page-header">
        <h2>{{ $book->title }} <br><small class="text-muted"> {{ (!is_null($book->author)) ? $book->author->name : '' }} </small></h2>
      </div>
    </div>

    {{-- Book Detail --}}
    <div class="row">
      <div class="col-lg-3">
        <div class="panel panel-default">
          @php
            if(!is_null($book->cover)){
              $image_url = asset('img').'/'.$book->cover;
            }else{
              // https://lorempixel.com/640/320/?
              // $image_url = 'https://lorempixel.com/300/400/'.$book->cover;
              $image_placeholder = asset('images/book_placeholder_300x400.jpg');
              $image_url = $image_placeholder;
            }
          @endphp          
          <img class="img-responsive card-img-top" src="{{ $image_url }}" alt="">
          
            @role(['member','admin'])
            <div class="panel-footer">
              @if(!is_null($book->pdf_file) && file_exists(public_path().'/pdf/'.$book->pdf_file))
                <a href="{{ url('/read').'/'.$book->id }}" class="btn btn-primary btn-lg btn-block">Baca</a>
              @else
                <a href="#" class="text-muted non-pdf"> <i>  PDF tidak tersedia</i> </a>
              @endif
            </div>
            @endrole

        </div>
        @if(Auth::guest())
            <div class="alert alert-warning">
              <strong>Baca Langsung Yuk!</strong> Silahkan login melalui <a href="{{ url('/login') }}" class="alert-link">tautan ini</a>.
            </div>
          @endif
      </div>

      <div class="col-lg-6">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#description" data-toggle="tab" aria-expanded="true">Deskripsi</a></li>
          <li class=""><a href="#detail" data-toggle="tab" aria-expanded="false">Detail</a></li>
          
        </ul>
        <div id="myTabContent" class="tab-content">
          <div class="tab-pane fade active in" id="description">
            <p>{{ $book->description }}</p>
          </div>
          <div class="tab-pane fade" id="detail">
            <table class="table table-striped">
                <tbody>
                  <tr>
                    <td>ISBN</td>
                    <td>{{ $book->isbn }}</td>
                  </tr>
                  <tr>
                    <td>Jumlah Halaman</td>
                    <td>{{ $book->total_page }}</td>
                  </tr>
                  <tr>
                    <td>Tanggal Terbit</td>
                    <td>{{ $book->publish_date }}</td>
                  </tr>
                  <tr>
                    <td>Penerbit</td>
                    {{-- <td>{{ $book->publisher->name }}</td> --}}
                  </tr>
                  <tr>
                    <td>Tempat Terbit</td>
                    <td>{{ $book->publish_place }}</td>
                  </tr>
                </tbody>
              </table>
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection