@extends('layouts.app')

@section('content')
<div class="container">

<div class="page-header">
	<h3>Contact Us</h3>
</div>
<p><h3>Badan Diklat Kejaksaan RI</h3></p>
<p>Jl.Harsono RM No.1 Ragunan, Pasar Minggu Jakarta Selatan 12550.</p>
<p>Telp/ Fax : (021)-7806004</P>

<div class="col-md-5">
	
    <div class="form-area">  
        <form role="form" action="{{ url('/contact-us/submit') }}" method="post">
        <br style="clear:both">
        			<input type="hidden" name="_token" value="{{ csrf_token() }}">
    				<div class="form-group">
						<input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="email" name="email" placeholder="Email" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="mobile" name="phone" placeholder="Mobile Number" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required>
					</div>
                    <div class="form-group">
                    <textarea class="form-control" type="textarea" name="message" id="message" placeholder="Message" maxlength="140" rows="7"></textarea>
                        <span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>                    
                    </div>
            
        <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Submit Form</button>
        </form>
    </div>
</div>
</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function(){ 
		    $('#characterLeft').text('140 characters left');
		    $('#message').keydown(function () {
		        var max = 140;
		        var len = $(this).val().length;
		        if (len >= max) {
		            $('#characterLeft').text('You have reached the limit');
		            $('#characterLeft').addClass('red');
		            $('#btnSubmit').addClass('disabled');            
		        } 
		        else {
		            var ch = max - len;
		            $('#characterLeft').text(ch + ' characters left');
		            $('#btnSubmit').removeClass('disabled');
		            $('#characterLeft').removeClass('red');            
		        }
		    });    
		});
  	</script>
@endsection
