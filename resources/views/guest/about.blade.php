@extends('layouts.app')

@section('content')
	<div class="container">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 post-title-block">
               
                <h1 class="text-center">Digilib Badiklat Kejaksaan RI</h1><br>

            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
                <p class="lead">Tentang Kami</p>
<p> Perpustakaan dan Dokumentasi Badan Pendidikan dan Pelatihan Kejaksaan RI adalah Perpustakaan yang merupakan bagian yang tidak dapat dipisahkan dari Badan Diklat Kejaksaan RI. Yang mana tujuan dari perpustakaan adalah untuk mendukung  dan menunjang pelaksanaan program Badan Diklat Kejaksaan RI sebagai pusat pendidikan dan pengembangan yang modern, berintegritas dan profesional yang bersendikan Satya Adhi Wicaksana.</p>
<p> Keberadaan perpustakaan Badan Diklat Kejaksaan RI untuk menjembatani kebutuhan siswa akan informasi dengan pengajar atau Widyaiswara yang mengarahkan dan membimbing siswa, sehingga perpustakaan tidak hanya menjadi jantung suatu lembaga pendidikan namun berkembang menjadi pusat pembelajaran, hubungan antara siswa dengan Widyasiwara tersebut menjadikan perpustakaan harus mampu membantu siswa dalam memenuhi kebutuhannya akan informasi.</p>
<p> Menyadari pentingnya peran tersebut menjadikan perpustakaan Badan Diklat Kejaksaan RI memusatkan perhatian sebagai sarana penunjang pendidikan, tempat penyimpanan, pengolahan dan penyebaran informasi serta pengembangan perpustakaan menuju perpustakaan Digital. (Cyber/ Digital Library) sesuai dengan tuntutan jaman akan informasi yang cepat dan akurat.</p>
                 <div class="well ">
                    <large>Syarat Ketentuan</large>
                </div>
<p>   1. Peminjam datang langsung keperpustakaan Badan Diklat Kejaksaan RI</p>
<p>   2. Cari buku yang  ingin dibaca /dipinjam secara langsung atau cari nomor panggil (call number) buku yang ingin dibaca melalui komputer pencarian (searching). Untuk pencarian dengan menggunakan komputer, pencarian dapat dilakukan berdasarkan judul, pengarang dan subjek.</p>
<p>   3. Peminjam menyerahkan buku yang akan dipinjam kepada petugas bagian loket peminjaman buku bersamaan dengan Kartu Anggota/Pinjam.</p>
<p>   4. Petugas mencatat transaksi peminjaman berupa Nomor Induk Buku (barcode), tanggal peminjaman, tanggal pengembalian, nama anggota peminjam.</p> 
<p>   5. Petugas menyerahkan buku yang akan dipinjam kepada peminjam bersamaan Kartu Anggota/ Pinjam Perpustakaan</p>
<p>   6. Proses transaksi peminjaman buku selesai.</p>

<p>Demi kepentingan akademis, Digital Library Badan Diklat Kejaksaan RI mempublish karya sivitas akademika. Bagi Anda yang mengutip isi dari hasil penelusuran media ini harap cantumkan sebagai sitasi pada karya Anda.</p>
<p>Digital Library Badan Diklat Kejaksaan tidak menanggung segala bentuk tuntutan hukum yang timbul atas pelanggaran hak cipta melalui media ini. ( Kepentingan di luar akademis menjadi tanggungjawab yang bersangkutan ).</p>

		 <div class="well ">
                    <large>Prosedur Pengembalian Buku</large>
                </div>
<p>   1. Peminjam menyerahkan buku yang akan dikembalikan kepada petugas bagian loket pengembalian buku</p>
<p>   2. Petugas mengecek data  transaksi peminjaman: tanggal pengembalian buku, apabila masa peminjaman telah melewati batas peminjaman, maka akan dikenakan denda peminjaman.</p>
<p>   3. Bila peminjam akan memperpanjang peminjaman kembali, maka petugas akan mencatat ke prosedur peminjaman buku.</p>
<p>   4. Petugas menyimpan buku yang akan dikembalikan ke rak buku dan menyerahkan Kartu Tanda Mahasiswa  kepada peminjam.</p>
<p>   5. Proses transaksi pengembalian buku selesai.</p>
      
           <div class="image-block">
                     <img src="https://static.pexels.com/photos/268455/pexels-photo-268455.jpeg" class="img-responsive" >
                 </div>


             </div>
            <div class="col-lg-4  col-md-4 col-sm-12">
                <div class="well">
                    <h2>Subscription Box</h2>
                    <p>Form Description Goes here</p>
                    <div class="input-group">
                    	<form action="{{ url('/subscribe') }}" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="text" name="email" class="form-control" placeholder="Email">
      <span class="input-group-btn">
        <button class="btn btn-default" type="submit">Subscribe</button>
      </span>
      </form>
    </div>
                </div>


                <div class="list-group">
                    <a class="list-group-item" href="#"> <h4 class="list-group-item-heading">List group item heading</h4> <p class="list-group-item-text">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p> </a>
                    <a class="list-group-item" href="#"> <h4 class="list-group-item-heading">List group item heading</h4> <p class="list-group-item-text">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p> </a>
                    <a class="list-group-item" href="#"> <h4 class="list-group-item-heading">List group item heading</h4> <p class="list-group-item-text">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p> </a> </div>
                <div class="well">
                    <div class="media"> <div class="media-left"> <a href="#"> <img data-src="holder.js/64x64" class="media-object" alt="64x64" style="width: 64px; height: 64px;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTY5MjIxZTM1NSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NjkyMjFlMzU1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMi41IiB5PSIzNi44Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true"> </a> </div> <div class="media-body"> <h4 class="media-heading">Media heading</h4> Cras sit amet nibh libero, in gravida nulla.</div> </div>
                    <div class="media"> <div class="media-left"> <a href="#"> <img data-src="holder.js/64x64" class="media-object" alt="64x64" style="width: 64px; height: 64px;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTY5MjIxZTM1NSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NjkyMjFlMzU1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMi41IiB5PSIzNi44Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true"> </a> </div> <div class="media-body"> <h4 class="media-heading">Media heading</h4> Cras sit amet nibh libero, in gravida nulla.</div> </div>
                    <div class="media"> <div class="media-left"> <a href="#"> <img data-src="holder.js/64x64" class="media-object" alt="64x64" style="width: 64px; height: 64px;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTY5MjIxZTM1NSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NjkyMjFlMzU1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMi41IiB5PSIzNi44Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true"> </a> </div> <div class="media-body"> <h4 class="media-heading">Media heading</h4> Cras sit amet nibh libero, in gravida nulla.</div> </div>
                </div>
            </div>
        </div>
      

    </div> <!-- /container -->
@endsection
