@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">

      @include('layouts._admin-menu')
      
      <div class="col-lg-10 col-md-10">
        <ul class="breadcrumb">
          <li><a href="{{ url('/home') }}">Dashboard</a></li>
          <li class="active">Obat</li>
        </ul>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h2 class="panel-title">Obat</h2>
          </div>

          <div class="panel-body">
            <p> 
                <a class="btn btn-primary" href="{{ url('/admin/books/create') }}">Tambah</a>
                <a class="btn btn-primary" href="{{ url('/admin/export/books') }}">Export</a>
            </p>
            {{-- $html->table(['id'=>'items-datatable', 'class'=>'table table-bordered']) --}} 
            <table class="table table-bordered" id="items-datatable">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Nama Obat</th>
                      <th>Kategori</th>
                      <th>Satuan</th>
                      <th>Napza</th>
                      <th>Stok</th>
                      <th>Lokasi</th>
                      <th></th>
                  </tr>
              </thead>
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  {{-- $html->scripts() --}}

  <script>
    $(function() {
        $('#items-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('medicines.index') !!}',
            columns: [
                { data: 'DT_Row_Index', orderable:false, searchable:false },
                { data: 'name', name: 'name' },
                { data: 'category.name', name: 'category.name' },
                { data: 'unit.name', name: 'unit.name' },
                { data: 'is_napza', name: 'is_napza' },
                { data: 'stock', name: 'stock' },
                { data: 'location', name: 'location' },
                { data: 'action', name: 'action', orderable: false, searchable:false }
            ],
            scrollX: true,
            columnDefs: [
                { targets: 1, width: '20%'},
                { targets: 7, width: '12%'},
            ],
            /*fixedColumns:   {
                rightColumns: 1
            }*/
        });
    });
  </script>

@endsection

