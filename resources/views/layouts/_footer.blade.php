<!--footer-->
<div class="page-header"></div>

<footer class="footer1">
    <div class="container">
        <div class="row"><!-- row -->
            <div class="col-lg-4 col-md-4">
                <ul class="list-unstyled clear-margins">
                    <li class="widget-container widget_nav_menu">
                        <ul>
                            <li><a  href="{{ url('/about') }}"><i class="fa fa-angle-double-right"></i> About Us</a></li>
                            <li><a  href="{{ url('/contact-us') }}"><i class="fa fa-angle-double-right"></i> Contact Us</a></li>
                            <li>
                                {!! Form::open(['url' => '/subscribe']) !!}
                                    <div class="form-group form-group-sm">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="text" name="email" class="form-control" placeholder="Email Anda">
                                    </div>
                                    <button type="submit" id="submit" name="submit" class="btn btn-default btn-sm">Subscribe</button>
                                {!! Form::close() !!}
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!--header-->

<div class="footer-bottom">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="copyright">
					© 2017, {{ config('app.name', 'Laravel') }}, All rights reserved
				</div>
			</div>

			<!-- <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="design">
                     <a href="#">Franchisee </a> |  <a target="_blank" href="http://www.webenlance.com">Web Design & Development by Webenlance</a>
                </div> -->
            <div class="col-xs-12 col-sm-6 col-md-offset-3 col-md-3 col-lg-offset-3 col-lg-3">
                <div class="social-icons pull-right">
                    <ul class="nomargin">
                        <a href="https://www.facebook.com/bootsnipp"><i class="fa fa-facebook-square fa-3x social-fb" id="social"></i></a>
                        <a href="https://twitter.com/bootsnipp"><i class="fa fa-twitter-square fa-3x social-tw" id="social"></i></a>
                        <a href="https://plus.google.com/+Bootsnipp-page"><i class="fa fa-google-plus-square fa-3x social-gp" id="social"></i></a>
                        <a href="mailto:bootsnipp@gmail.com"><i class="fa fa-envelope-square fa-3x social-em" id="social"></i></a>
                    </ul>
                </div>
			</div>
		</div>
	</div>
</div>