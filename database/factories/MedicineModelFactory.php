<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Medicine::class, function (Faker\Generator $faker) {

	$unit = App\Unit::orderByRaw('RAND()')->first();
	$category = App\Category::orderByRaw('RAND()')->first();
   
    return [
        'name' => $faker->streetName,
        'unit_id' => $unit->id,
        'category_id' => $category->id,
        'stock' => $faker->randomDigitNotNull,
        'is_napza' => random_int(0, 1),
        'location' => $faker->address,
    ];
});
