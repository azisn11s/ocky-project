<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('books', function (Blueprint $table) {
            $table->integer('category_id')->unsigned()->after('cover');
            $table->integer('publisher_id')->unsigned();
            $table->string('isbn', 100)->nullable()->unique();
            $table->string('publish_place', 70)->nullable();
            $table->dateTime('publish_date')->nullable();
            $table->integer('total_page')->nullable();
            $table->integer('seen_count')->nullable();
            $table->integer('download_count')->nullable();
            $table->text('description')->nullable();

            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('publisher_id')->references('id')->on('publishers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('books', function (Blueprint $table) {
            $table->dropForeign('books_category_id_foreign');
            $table->dropForeign('books_publisher_id_foreign');
            $table->dropColumn('category_id');
            $table->dropColumn('publisher_id');
            $table->dropColumn('isbn');
            $table->dropColumn('publish_place');
            $table->dropColumn('publish_date');
            $table->dropColumn('total_page');
            $table->dropColumn('seen_count');
            $table->dropColumn('download_count');
            $table->dropColumn('description');
        });
    }
}
