<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use App\Medicine;
use App\Category;
use App\Unit;

class MedicineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $medicines = Medicine::with(['unit', 'category'])->orderBy('id', 'desc');
            return Datatables::of($medicines)
            	->addIndexColumn()
            	->editColumn('is_napza', function($medicine){
            		if($medicine->is_napza)
            			return '<span class="btn btn-xs btn-warning">Napza</span>';

            		return '<span class="btn btn-xs btn-success">Bukan</span>';                    
                }) 
                ->addColumn('action', function($medicine){
                    return view('datatable._action', [
                        'model'           => $medicine,
                        'form_url'        => route('medicines.destroy', $medicine->id),
                        'edit_url'        => route('medicines.edit', $medicine->id),
                        'confirm_message' => 'Yakin mau menghapus ' . $medicine->name . '?'
                    ]);
                })                
                ->make(true);
        }

        $html = $htmlBuilder
        	  ->addColumn(['data'=> 'DT_Row_Index', 'title'=> 'No', 'orderable'=>false, 'searchable'=>false, 'width'=>'5px'])
            ->addColumn(['data' => 'name', 'name'=>'name', 'title'=>'Nama Barang'])
            ->addColumn(['data' => 'category.name', 'name'=>'category.name', 'title'=>'Kategori'])
            ->addColumn(['data' => 'unit.name', 'name'=>'unit.name', 'title'=>'Satuan'])
            ->addColumn(['data' => 'is_napza', 'name'=>'is_napza', 'title'=>'Napza'])
            ->addColumn(['data' => 'stock', 'name'=>'stock', 'title'=>'Stok'])
            ->addColumn(['data' => 'location', 'name'=>'location', 'title'=>'Lokasi'])
            ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false])
            ->parameters([
            	'autoWidth'=> false,
            	'scrollX'=> true,
            ]);
	
		// dd($html->scripts());
        // return view('items.index')->with(compact('html'));
            return view('medicines.index');
    }
}
