<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Category;

class SearchController extends Controller
{
    
    public function index(Request $request)
    {
    	$keywords = [];

    	if($request->has('title'))
    		$keywords['title'] = $request->get('title');

    	$title = $keywords['title'];

    	if($request->has('category_id'))
    		array_push($keywords, ['category_id'=>$request->get('category_id')]);

    	$books = Book::where('title', 'like', '%' . $keywords['title'] . '%')->get();

    	return view('search.index')->with(compact('books', 'title'));
    }

    public function category($id)
    {
    	$category = Category::find($id);
    	$books = Category::find($id)->books()->paginate(12);
    	
    	return view('search.category')->with(compact('books','category'));
    }
}
