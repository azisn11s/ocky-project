<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use App\Book;
use App\Contact;
use Laratrust\LaratrustFacade as Laratrust;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;


class GuestController extends Controller
{

    public function index()
    {
        $books = Book::orderby('created_at','desc')->paginate(8);

        return view('guest.index')->with(compact('books'));
    }

    public function detail($id)
    {
        $book = Book::find($id);

        return view('guest.detail')->with(compact('book'));
    }

    public function read($id)
    {
        $book = Book::find($id);

        $filename = $book->pdf_file;
        $path = public_path() . DIRECTORY_SEPARATOR . 'pdf/' . $filename;

        return response()->file($path);
    }

    public function aboutUs()
    {
        return view('guest.about');
    }

    public function contactUs()
    {
        return view('guest.contact');
    }

    public function submitContactUs(Request $request)
    {
        $this->validate($request, [
            'name'=> 'required|max:70',
            'email'=> 'required|email|max:70',
            'phone'=> 'required|max:20',
            'subject'=> 'max:100',
            'message'=> 'required|max:140'
        ]);

        $contactUs = Contact::create($request->all());

        Mail::send('guest.email-contact-us', compact('contactUs'), function ($m) use ($contactUs) {
            $m->to('digilibkejaksaan@gmail.com', 'Admin Kejaksaan')
            // $m->to('azisnovian.kom48@gmail.com', 'Admin Kejaksaan')
            ->from($contactUs->email, $contactUs->name)
            ->subject('DIGILIB - Contact Us');
        });

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil mengirim pesan ke Digilib."
        ]);

        return redirect()->back();

    }

    public function index_old(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $books = Book::with(['author', 'category', 'publisher']);
            return Datatables::of($books)
                ->addColumn('stock', function($book){
                    return $book->stock;
                })
                /*->addColumn('action', function($book){
                    if (Laratrust::hasRole('admin')) return '';
                    return '<a class="btn btn-xs btn-primary" href="'.route('guest.books.borrow', $book->id).'">Pinjam</a>';
                })*/
                ->make(true);
        }

        $html = $htmlBuilder
            ->addColumn(['data' => 'title', 'name'=>'title', 'title'=>'Judul'])
            ->addColumn(['data' => 'category.name', 'name'=>'category.name', 'title'=>'Kategori'])
            ->addColumn(['data' => 'author.name', 'name'=>'author.name', 'title'=>'Penulis'])
            ->addColumn(['data' => 'publisher.name', 'name'=>'publisher.name', 'title'=>'Penerbit']);
            // ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false]);

        return view('guest.index')->with(compact('html'));
    }
}
