<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter;
use Exception;

class MailchimpsController extends Controller
{
    

    public function postSubscribe(Request $request)
    {
    	$this->validate($request, [
	    	'email' => 'required|email',
        ]);


        try {

        	if (!Newsletter::isSubscribed($request->input('email'))) {
        	
	        	Newsletter::subscribe($request->input('email'));

	        	return redirect()->back()->with('success','Email Subscribed successfully');
	        }

	        return redirect()->back()->with('error','Email is Already Subscribed');

        } catch (Exception $e) {

        	return redirect()->back()->with('error','Error from MailChimp. Message : '.$e->getMessage());
        }
    }
}
