<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use App\Item;
use App\Category;
use App\Unit;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $items = Item::with(['unit', 'category'])->orderBy('id', 'desc');
            return Datatables::of($items)
            	->addIndexColumn()
                ->addColumn('action', function($item){
                    return view('datatable._action', [
                        'model'           => $item,
                        'form_url'        => route('items.destroy', $item->id),
                        'edit_url'        => route('items.edit', $item->id),
                        'confirm_message' => 'Yakin mau menghapus ' . $item->name . '?'
                    ]);
                })                
                ->make(true);
        }

        $html = $htmlBuilder
        	  ->addColumn(['data'=> 'DT_Row_Index', 'title'=> 'No', 'orderable'=>false, 'searchable'=>false, 'width'=>'5px'])
            ->addColumn(['data' => 'name', 'name'=>'name', 'title'=>'Nama Barang'])
            ->addColumn(['data' => 'category.name', 'name'=>'category.name', 'title'=>'Kategori'])
            ->addColumn(['data' => 'unit.name', 'name'=>'unit.name', 'title'=>'Satuan'])
            ->addColumn(['data' => 'stock', 'name'=>'stock', 'title'=>'Stok'])
            ->addColumn(['data' => 'location', 'name'=>'location', 'title'=>'Lokasi'])
            ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false])
            ->parameters([
            	'autoWidth'=> false,
            	'scrollX'=> true,
            ]);
	
		// dd($html->scripts());
        // return view('items.index')->with(compact('html'));
            return view('items.index');
    }
}
