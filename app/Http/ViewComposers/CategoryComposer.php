<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Category;

/**
* 
*/
class CategoryComposer
{

	private $main_categories;
	
	function __construct()
	{
		$this->main_categories = Category::all();
	}

	public function compose(View $view)
	{
		$view->with('main_categories', $this->main_categories);
	}

}