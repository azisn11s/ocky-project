<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class Category extends Model
{
    protected $fillable = ['name'];

    public function books()
    {
        return $this->hasMany(Book::class);
    }

    public static function boot()
    {
        parent::boot();

        self::deleting(function($category) {
            // mengecek apakah penulis masih punya buku
            if ($category->books->count() > 0) {
                // menyiapkan pesan error
                $html = 'Kategori tidak bisa dihapus karena keterkaitan dengan buku : ';
                $html .= '<ul>';
                foreach ($category->books as $book) {
                    $html .= "<li>$book->title</li>";
                }
                $html .= '</ul>';

                Session::flash("flash_notification", [
                    "level"=>"danger",
                    "message"=>$html
                ]);

                // membatalkan proses penghapusan
                return false;
            }
        });
    }
    
}
